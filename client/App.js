import React, {Component} from 'react';
import ReactDom from 'react-dom';
import './style/style.css';

import Header from './components/Header';
import Body from './components/Body';





class App extends Component {

	render() {

		return(
			<div>
				<Header />
				<Body />
			</div>
		);
	}

}

ReactDom.render(<App />, document.getElementById('root'));