import React, {Component} from 'react';
import User from './User';

class UserList extends Component {

	constructor(props){
		super(props);	
		this.state = {

			userList: this.props.users
		}
	}

	render() {

		let userList = [];
		let key = 1;
		for(let i = 0; i < this.state.userList.length; i++) {
			let user = this.state.userList[i];
			userList.push(<User details={user} key={key++} />);
		}
		return (
			<div className="container user-list">
				<a className="btn btn-xs pull-right" onClick={this.props.toggleUserForm}>X</a>
				<h1> </h1>
				{userList}
			</div>
		);
	}
}

export default UserList;