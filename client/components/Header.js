import React, {Component} from 'react';


class Header extends Component {

	render() {

		let headerStyle = {
			textAlign: 'center',
			border: '2px solid black',
			boxShadow: '0 4px 4px -1px #0A1B2A'
		};

		return(
			<div className="container-fluid header" style={headerStyle}>
   				<div id="face">
   					<h1>FaceApp</h1>
   					<h3>An Application that knows your face</h3>
   				</div>
			</div>
		);
	}
}

export default Header;
