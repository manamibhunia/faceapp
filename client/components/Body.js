import React, {Component} from 'react';
import $ from 'jquery';
import UserList from './UserList';
import Spinner from './Spinner';
import ReactTimeout from 'react-timeout';

class Body extends Component {

	constructor(props){
		super(props);	
		this.state = {

			isUSerListVisible: false,
			users: null,
			picture: null,
			isBusy: false,
			serverError: <span />
		}
	}

	toggleUserForm(event) {
		event.preventDefault();
		this.setState({
			isUSerListVisible: !this.state.isUSerListVisible
		});
	}

	uploadPic(event) {
		event.stopPropagation();
    	event.preventDefault();
    	var _this = this;
    	this.setState({
    		isBusy: true,
    	});
    	setTimeout(function(){

		    $.ajax({
		        url: 'file_upload?name=' + _this.state.picture[0].name,
		        type: 'POST',
		        cache: false,
		        dataType: 'json',
		        processData: false,
		        success: function(data, textStatus, jqXHR)
		        {
		        	if(!data.length) {
			            _this.setState({
							serverError: (
								<div className="alert alert-danger">
									<strong>Sorry, No Match Found!</strong>
								</div>
							)
			            });
		        	}
					_this.setState({
						users: data,
						isBusy:false,
						isUSerListVisible: !_this.state.isUSerListVisible
					});
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		            console.log('ERRORS: ' + textStatus);
		            this.setState({
		            	isBusy:false,
						serverError: (<div className="alert alert-danger">
							<strong> Sorry, No Match Found!</strong>
						</div>)
		            });
		        }
		    });
             
        }.bind(this),3500);
	}

	handleFileSelect(event) {
		this.setState({
			picture: event.target.files
		})
	}

	render() {

		let userList = '';
		let uploadForm = (
		 	<div id="upload-form">
				<div className="form-horizontal">
					<div className="form-group">
						<label htmlFor="photo" className="col-sm-4 control-label">Upload a Photo</label>
						<div className="col-sm-8">
							<input type="file" className="form-control" name="photo" id="photo" onChange={this.handleFileSelect.bind(this)}/>
						</div>
					</div>
					<div className="form-group">
						
					</div>
				</div>
				<a className="form-control upload-btn btn btn-primary btn-sm" onClick={this.uploadPic.bind(this)}>Upload</a>
			</div>
		);
		if(this.state.isUSerListVisible) {
			userList = <UserList users={this.state.users} toggleUserForm={this.toggleUserForm.bind(this)} />;
			uploadForm = '';
		}
		
	
		let spinner = '';
		if(this.state.isBusy) {
			spinner = <Spinner />;
		}
		return(
			 <div className="container-fluid main">
			 	{uploadForm}

				<div className="container-fluid">
					{userList}
					{spinner}
				</div>
				{this.state.serverError}
			 </div>
		);
	}
}

export default Body;