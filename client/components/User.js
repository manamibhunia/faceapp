import React from 'react';

class User extends React.Component {
	
	render() {

		let details = this.props.details;
		let image = '';
		let firstName = '';
		let lastName = '';
		let gender = '';
		let phone = '';
		let email = '';
		let street = '';
		let city = '';
		let state = '';
		let postcode = '';
		let occupation = '';

		let divStyle = {
			background: '#E1ADA4',
			border: '1px solid gray',
			opacity: '0.8',
			font: 'black',
			boxShadow: '0 4px 2px -2px gray'
		};
		let imagegStyle={
			height:'210px', 
			width:'150px'
		};

		if(details) {
			image = (details.picture && details.picture.thumbnail) ? details.picture.thumbnail : '';
			firstName = (details.name && details.name.first) ? details.name.first : '';
			lastName = (details.name && details.name.last) ? details.name.last : '';
			gender = (details.gender) ? details.gender : '';
			phone = (details.phone) ? details.phone : '';
			email = (details.email) ? details.email : '';
			street = (details.address && details.address.street) ? details.address.street : '';
			city = (details.address && details.address.city) ? details.address.city : '';
			state = (details.address && details.address.state) ? details.address.state : '';
			postcode = (details.address && details.address.postcode) ? details.address.postcode : '';
			occupation = (details.occupation) ? details.occupation : '';
		}
		
		return (
			<div className="row user-row" style={divStyle}>
				<div className="col-md-6">
					<p> Profile with best Match found!</p>
					<img style={imagegStyle} src={image} /> 
				</div>

				<div className="col-md-6" >
					<p>Name : {firstName} {lastName}</p>
					<p>Gender : {gender}</p>
					<p>Email : {email}</p>
					<p>Phone : {phone}</p>			
					<p>Address : {street},  {city}, {state} </p>
					<p>Zip : {postcode}</p>
					<p>Occupation : {occupation}</p>

				</div>
			</div>
		);
	}
}

export default User;